﻿angular.module('zyppo.controllers.activities', [])

.controller('ActivitiesCtrl', function (ActivitiesService, $rootScope, $scope, $ionicSideMenuDelegate, $ionicPopup, $state) {
    $scope.activities = true;
    var id = window.localStorage.getItem("idUsuario");
    try {
        ActivitiesService.myActivities(id).then(function (response) {
            $scope.notifications = convertNotifications(response.data.notificacoes);
            $scope.activitiesList = convertNotifications(response.data.atividades);
        });
    } catch (e) {
        alert(e);
    }


    $scope.notificationClick = function () {
        $scope.activities = false;
    }

    $scope.activitiesClick = function () {
        $scope.activities = true;
    }

    function convertNotifications(notifications) {
        for (i = 0; i < notifications.length; i++) {
            var notification = notifications[i];
            switch (notification.acao) {
                case 'resgate': notification.image = 'images/rescue.svg'; notification.message = 'Seu voucher ' + notification.codigo + ' resgatado no jogo ' + notification.nome + ' foi utilizado.'; break;
                case 'adicionado': notification.message = 'Você foi adicionado ao jogo ' + notification.nome; break;
                case 'credito': notification.message = 'Você recebeu ' + notification.pontos + ' pontos no jogo ' + notification.nome + '.'; break;
                case 'entrou': notification.image = 'images/joystick.svg'; notification.message = 'Você acaba de entrar no jogo ' + notification.nome + '. Comece a jogar!'; break;
                case 'pontos': notification.image = 'images/y.svg'; notification.message = 'Você ganhou ' + notification.pontos + ' pontos no jogo ' + notification.nome + '.'; break;
                case 'saiu': notification.image = 'images/congrats.svg'; notification.message = 'Parabéns! Você concluiu o jogo ' + notification.nome + ' e conquistou seu prêmio.'; break;
                default: ''
            }

            // if (notification.data != null) {
            //     notification.data = new Date(notification.data);
            // }
        }
        return notifications;
    }

})