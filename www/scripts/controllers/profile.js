﻿angular.module('zyppo.controllers.profile', [])

.controller('ProfileCtrl', function ($rootScope, $scope, $ionicModal, $ionicSideMenuDelegate, $ionicPopup, $state, AccountService, HtmlsService) {
    $scope.imageProfile = 'images/avatar.png';
    var idUser = window.localStorage.getItem("idUsuario");

    try {
        AccountService.getUser(idUser).then(function (response) {
            $scope.cpf = formatCpf(response.data.cpf, '000.000.000-00');
            $scope.email = response.data.email;
            $scope.name = response.data.nome;
            if (response.data.avatar)
                $scope.profileImage = 'http://app.zypo.com.br/img/avatar/' + response.data.avatar;
            else
                $scope.profileImage = 'images/avatar.png';

        });
    } catch (e) {
        alert(e);
    }

    $scope.contactClick = function () {
        window.open('http://zypo.com.br/#fale', '_blank', 'location=yes');
    }

    $scope.aboutClick = function () {
        $state.go('app.htmls', { page: 'about' });
    }

    $scope.termsClick = function () {
        $state.go('app.htmls', { page: 'terms' });
    }

    //formata de forma generica os campos
    function formatCpf(campo, Mascara) {
        var boleanoMascara;

        exp = /\-|\.|\/|\(|\)| /g
        campoSoNumeros = campo.toString().replace(exp, "");

        var posicaoCampo = 0;
        var NovoValorCampo = "";
        var TamanhoMascara = campoSoNumeros.length;;

        for (i = 0; i <= TamanhoMascara; i++) {
            boleanoMascara = ((Mascara.charAt(i) == "-") || (Mascara.charAt(i) == ".")
                                || (Mascara.charAt(i) == "/"))
            boleanoMascara = boleanoMascara || ((Mascara.charAt(i) == "(")
                                || (Mascara.charAt(i) == ")") || (Mascara.charAt(i) == " "))
            if (boleanoMascara) {
                NovoValorCampo += Mascara.charAt(i);
                TamanhoMascara++;
            } else {
                NovoValorCampo += campoSoNumeros.charAt(posicaoCampo);
                posicaoCampo++;
            }
        }
        return NovoValorCampo;
    }

    $scope.changePictureClick = function () {
        navigator.camera.getPicture(showImage, onFail, {
            destinationType: Camera.DestinationType.DATA_URL,
            sourceType: Camera.PictureSourceType.PHOTOLIBRARY,
            mediaType: Camera.MediaType.PICTURE,
            correctOrientation: true
        });
    }
    function showImage(imageURI) {
        var image = document.getElementById('imgProfile');
        image.src = image.src = "data:image/jpeg;base64," + imageURI;
        var idUser = window.localStorage.getItem("idUsuario");

        AccountService.changePicture(idUser, image.src).then(function (response) {
        });

    }

    function onFail(message) {
        //alert('Failed because: ' + message);
    }

    $scope.editProfileClick = function () {
        $state.go('app.register', { cpf: 'edit' }, { reload: true })
    }

     $scope.logoffClick = function () {
        console.log('clear');
        window.localStorage.clear();
        $state.go('app.firstAccess');
    }

    // $scope.logoffClick = function () {
    //     $state.go('app.firstAccess');
    // }

})