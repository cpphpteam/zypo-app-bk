﻿angular.module('zyppo.controllers.myGames', [])

.controller('MyGamesCtrl', function ($rootScope, $scope, $ionicSideMenuDelegate, $ionicPopup, $state, GameService) {
    $ionicSideMenuDelegate.canDragContent(true);
    $rootScope.showFooter = true;
    $rootScope.uiRouterState = $state;
    $scope.model = {};
    $scope.orderBy = $state.params.orderBy;
    var id = window.localStorage.getItem("idUsuario");


    $scope.doRefresh = function () {
        GameService.myGames(id).then(function (result) {
            if (result.data && typeof result.data === "object") {
                $scope.games = result.data;
                $scope.gamesData = result.data;
            }

            else {
                $scope.games = new Array();
                $scope.gamesData = $scope.games;

            }
            $scope.$broadcast('scroll.refreshComplete');
        });
    }

    $scope.doRefresh();

    $scope.validatePoints = function (userPoint, gamePoint) {
        return parseInt(gamePoint) > parseInt(userPoint);
    }


    getGameByName = function (name) {
        return $scope.gamesData.filter(
            function (game) { return game.nome.toLowerCase().indexOf(name.toLowerCase()) != -1 }
        );
    }

    $scope.searchPress = function () {
        if ($scope.model.searchText.length == 0) {
            $scope.games = $scope.gamesData;
        }
    }

    $scope.search = function () {
        $scope.games = getGameByName($scope.model.searchText);
    }

    $scope.gameClick = function (game) {
        $state.go('app.myPrizes', { id: game.id });
    }
})