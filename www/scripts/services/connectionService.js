﻿angular.module('zyppo.services.connectionService', [])

      .factory('ConnectionService', function (constants, $q, $http, $ionicLoading) {

        // We use promises to make this api asynchronous. This is clearly not necessary when using in-memory data
        // but it makes this service more flexible and plug-and-play. For example, you can now easily replace this
        // service with a JSON service that gets its data from a remote server without having to changes anything
        // in the modules invoking the data service since the api is already async.

        return {
            isConnected: function () {
                var deferred = $q.defer();
                //try {
                   // var networkState = navigator.connection.type;

                    //if (networkState == "none") {
                        deferred.resolve(true);
                    //} else {
                        //deferred.resolve(true);
                    //}
                //} catch (e) {

                    //setTimeout(function () {
                       // try {
                            //var networkState = navigator.connection.type;

                            //if (networkState == "none") {
                                //if (true) {
                                //deferred.resolve(false);


                            //} else {
                                //deferred.resolve(true);
                            //}
                       // } catch (e) {
                           // alert(e);
                        //}
                    //}, 1000);
                //}

                return deferred.promise;

            },
        }

    });