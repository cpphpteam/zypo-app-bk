﻿angular.module('zyppo.services.account', [])

    .factory('AccountService', function (constants, $q, $http, $ionicLoading, CacheService) {

        // We use promises to make this api asynchronous. This is clearly not necessary when using in-memory data
        // but it makes this service more flexible and plug-and-play. For example, you can now easily replace this
        // service with a JSON service that gets its data from a remote server without having to changes anything
        // in the modules invoking the data service since the api is already async.

        return {
            save: function (account) {
                var deferred = $q.defer();
                $ionicLoading.show({
                    template: 'Carregando...',
                    duration: 3000
                });
                
                $http.post(constants.BASE_URL + 'usuario/save', account).then(function (response) {

                    deferred.resolve(response);

                    $ionicLoading.hide();
                })
                return deferred.promise;
            },
            edit: function (account) {
                var deferred = $q.defer();
                $ionicLoading.show({
                    template: 'Carregando...',
                    duration: 3000
                });
                $http({
                    method: 'POST',
                    url: 'http://app.zypo.com.br/usuario/edit',
                    data: $.param(account),
                    headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
                }).then(function (response) {
                    deferred.resolve(response);
                    $ionicLoading.hide();
                })
                return deferred.promise;
            },

            changePicture: function (id, image) {
                var deferred = $q.defer();
                $ionicLoading.show({
                    template: 'Carregando...',
                    duration: 3000
                });
                $http({
                    method: 'POST',
                    url: 'http://app.zypo.com.br/usuario/alterar-avatar',
                    data: $.param({ id: id, img: image }),
                    headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
                }).then(function (response) {
                    deferred.resolve(response);
                    $ionicLoading.hide();
                }, function (response) {
                });
                return deferred.promise;
            },
            recoverPassword: function (email) {
                var deferred = $q.defer();
                $ionicLoading.show({
                    template: 'Carregando...',
                    duration: 3000
                });
                $http.post(constants.BASE_URL + 'usuario/solicitar-recuperar-senha', { email: email }).then(function (response) {

                    deferred.resolve(response);

                    $ionicLoading.hide();
                })
                return deferred.promise;
            },
            getUser: function (id) {
                var deferred = $q.defer();
                CacheService.getCache('getUser', deferred).then(function (hasCache) {
                    if (!hasCache) {
                        $ionicLoading.show({
                            template: 'Carregando...',
                            duration: 3000
                        });
                        $http.post(constants.BASE_URL + 'usuario/find/id/' + id, {}).then(function (response) {

                            deferred.resolve(response);

                            $ionicLoading.hide();
                        });
                    }
                });
                return deferred.promise;
            },

            authenticationFacebook: function (idfacebook, cpf, email) {
                try {
                    var deferred = $q.defer();
                    $ionicLoading.show({
                        template: 'Carregando...',
                        duration: 3000
                    });
                    $http({
                        method: 'POST',
                        url: 'http://app.zypo.com.br/usuario/face-book-login',
                        data: $.param({ cpf: cpf, idfacebook: idfacebook, email: email }),
                        headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
                    }).then(function (response) {
                        //alert(response);
                        deferred.resolve(response);
                        $ionicLoading.hide();
                    })
                    return deferred.promise;
                } catch (e) {
                    alert(e)
                }

            },

            authentication: function (cpf, password) {
                try {
                    var deferred = $q.defer();
                    $ionicLoading.show({
                        template: 'Carregando...',
                        duration: 3000
                    });
                    $http({
                        method: 'POST',
                        url: 'http://app.zypo.com.br/usuario/login',
                        data: $.param({ cpf: cpf, senha: password }),
                        headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
                    }).then(function (response) {
                        //alert(response);
                        deferred.resolve(response);
                        $ionicLoading.hide();
                    })
                    return deferred.promise;
                } catch (e) {
                    alert(e)
                }

            }
        }

    });