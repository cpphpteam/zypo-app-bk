﻿angular.module('zyppo.services.prizesService', [])

    .factory('PrizesService', function (constants, $q, $http, $ionicLoading, CacheService) {

        // We use promises to make this api asynchronous. This is clearly not necessary when using in-memory data
        // but it makes this service more flexible and plug-and-play. For example, you can now easily replace this
        // service with a JSON service that gets its data from a remote server without having to changes anything
        // in the modules invoking the data service since the api is already async.

        return {
            getById: function (idUser, idGame) {               

                var deferred = $q.defer();
                CacheService.getCache('getById', deferred).then(function (hasCache) {
                    if (!hasCache) {
                        $ionicLoading.show({
                            template: 'Carregando...',
                            duration: 3000
                        });
                        $http.get(constants.BASE_URL + 'jogos/visualizar-jogo/jogoId/' + idGame + '/usuarioId/' + idUser).then(function (response) {
                            deferred.resolve(response);
                            CacheService.setCache('getById', JSON.stringify(response));

                            $ionicLoading.hide();
                        })
                    }                    
                });
                return deferred.promise;
            },

            create: function (idVoucher, idUser) {
                var deferred = $q.defer();

                $http({
                    method: 'POST',
                    url: 'http://app.zypo.com.br/jogos/resgate',
                    data: $.param({ premioId: idVoucher, usuarioId: idUser }),
                    headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
                }).then(function (response) {
                    deferred.resolve(response);
                    $ionicLoading.hide();
                })

                return deferred.promise;
            }
        }

    });