﻿angular.module('zyppo.services.firstAccess', [])

    .factory('FirstAccessService', function (constants, $q, $http, $ionicLoading) {

        // We use promises to make this api asynchronous. This is clearly not necessary when using in-memory data
        // but it makes this service more flexible and plug-and-play. For example, you can now easily replace this
        // service with a JSON service that gets its data from a remote server without having to changes anything
        // in the modules invoking the data service since the api is already async.

        return {
            checkAccount: function (cpf) {
                var deferred = $q.defer();
                var config = {
                    headers: {
                         'Content-Type': 'Access-Control-Allow-Origin: *;application/x-www-form-urlencoded;charset=utf-8;'
                     }
                 }
                $ionicLoading.show({
                    template: 'Carregando...',
                    duration: 3000
                });
                // $http.post('https://zypo.herokuapp.com/usuario/verifica-cpf/cpf/' + cpf).then(function (response) {
                $http.post(constants.BASE_URL + 'usuario/verifica-cpf/cpf/' + cpf, config).then(function (response) {

                    if (response.data == "não cadastrado") {
                        deferred.resolve("not_registered");
                    } else if (response.data == 'ja cadastrado') {
                        deferred.resolve("registered");
                    } else if (response.data.replace(new RegExp('[ÁÀÂÃ]', 'gi'), 'a') == "O CPF digitado e invalido") {
                        deferred.resolve("invalid_cpf");
                    }
                    $ionicLoading.hide();
                })
                return deferred.promise;
            },
        }

    });