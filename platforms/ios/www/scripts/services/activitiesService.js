﻿angular.module('zyppo.services.activitiesService', [])

    .factory('ActivitiesService', function ($q, $http, $ionicLoading, CacheService) {

        // We use promises to make this api asynchronous. This is clearly not necessary when using in-memory data
        // but it makes this service more flexible and plug-and-play. For example, you can now easily replace this
        // service with a JSON service that gets its data from a remote server without having to changes anything
        // in the modules invoking the data service since the api is already async.

        return {
            myActivities: function (idUser) {
                var deferred = $q.defer();
                CacheService.getCache('myActivities', deferred).then(function (hasCache) {
                    if (!hasCache) {
                        $ionicLoading.show({
                            template: 'Carregando...',
                            duration: 3000
                        });
                        $http.post('http://zypo.herokuapp.com/usuario/atividades-notificacoes/id/' + idUser).then(function (response) {
                            deferred.resolve(response);
                            CacheService.setCache('myActivities', JSON.stringify(response));
                            $ionicLoading.hide();
                        })
                    }
                });
                return deferred.promise;
            },
        }

    });