﻿angular.module('zyppo.services.cacheService', [])

    .factory('CacheService', function ($q, $http, $ionicLoading, ConnectionService, $ionicPopup, $state) {

        // We use promises to make this api asynchronous. This is clearly not necessary when using in-memory data
        // but it makes this service more flexible and plug-and-play. For example, you can now easily replace this
        // service with a JSON service that gets its data from a remote server without having to changes anything
        // in the modules invoking the data service since the api is already async.


        //return {
        //    getCache: function (cacheName, deferred) {

        //        var deferred2 = $q.defer();
        //        deferred2.resolve(false);

        //        return deferred2.promise;
        //    },

        //    setCache: function (cacheName, data) {
        //        window.localStorage.setItem(cacheName, data);
        //    },
        //}

        return {
            getCache: function (cacheName, deferred) {

                var deferred2 = $q.defer();
                try {
                    ConnectionService.isConnected().then(function (isConnected) {
                        if (!isConnected) {
                            var dataStorage = window.localStorage.getItem(cacheName);
                            if (!dataStorage) {
                                $ionicPopup.alert({
                                    title: 'Ocorreu um erro',
                                    template: 'Aparelho sem conexão com a internet'
                                });
                                $state.go('app.noConnection');
                            }
                            else {
                                var data = JSON.parse(dataStorage);
                                deferred.resolve(data);
                                deferred2.resolve(true);
                            }

                        }
                        else {
                            deferred2.resolve(false);
                        }
                    });

                    return deferred2.promise;
                }

                catch (e) {
                    alert(e);
                }

            },

            setCache: function (cacheName, data) {
                window.localStorage.setItem(cacheName, data);
            },
        }

    });