﻿// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
angular.module('zyppo', ['ionic', 'zyppo.services.htmlsService', 'zyppo.controllers.htmls', 'zyppo.services.cacheService', 'zyppo.controllers.noConnection', 'zyppo.services.connectionService', 'ionic-native-transitions', 'zyppo.services.activitiesService', 'zyppo.controllers', 'zyppo.services.prizesService', 'zyppo.services.voucherService', 'zyppo.controllers.myPrizes', 'zyppo.controllers.activities', 'zyppo.controllers.profile', 'zyppo.controllers.vouchersDetail', 'zyppo.controllers.myVouchers', 'zyppo.controllers.myGames', 'zyppo.controllers.firstAccess', 'zyppo.controllers.recoverPassword', 'zyppo.controllers.register', 'zyppo.controllers.login', 'zyppo.services.account', 'zyppo.services.gameService', 'zyppo.services.firstAccess', 'ngMask'])

.run(function ($ionicPlatform) {
    $ionicPlatform.ready(function () {
        // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
        // for form inputs)
        if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
            cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
        }
        if (window.StatusBar) {
            // org.apache.cordova.statusbar required
            StatusBar.styleDefault();
        }
    });
})

.config(function ($compileProvider, $stateProvider, $urlRouterProvider, $ionicNativeTransitionsProvider) {

    $compileProvider.aHrefSanitizationWhitelist(/^\s*(https?|ftp|mailto|file|ghttps?|ms-appx|x-wmapp0):/);
    // // Use $compileProvider.urlSanitizationWhitelist(...) for Angular 1.2
    $compileProvider.imgSrcSanitizationWhitelist(/^\s*(https?|ftp|file|ms-appx|x-wmapp0):|data:image\//);

    $stateProvider

    .state('app', {
        url: "/app",
        abstract: true,
        templateUrl: "templates/menu.html",
        controller: 'AppCtrl'
    })
             .state('app.firstAccess', {
                 url: "/firstAccess",
                 views: {
                     'menuContent': {
                         templateUrl: "templates/first-access.html",
                         controller: 'FirstAccessCtrl'
                     }
                 }
             })

        .state('app.recoverPassword', {
            url: "/recoverPassword",
            views: {
                'menuContent': {
                    templateUrl: "templates/recover-password.html",
                    controller: 'RecoverPassword'
                }
            }
        })
         .state('app.myPrizes', {
             url: "/myPrizes/:id",
             views: {
                 'menuContent': {
                     templateUrl: "templates/my-prizes.html",
                     controller: 'MyPrizes'
                 }
             }
         })

     .state('app.register', {
         url: "/register/:cpf",
         views: {
             'menuContent': {
                 templateUrl: "templates/register.html",
                 controller: 'RegisterCtrl'
             }
         }
     })

     .state('app.myGames', {
         url: "/myGames/:orderBy",
         views: {
             'menuContent': {
                 templateUrl: "templates/my-games.html",
                 controller: 'MyGamesCtrl'
             }
         }
     })


     .state('app.noConnection', {
         url: "/noConnection",
         views: {
             'menuContent': {
                 templateUrl: "templates/no-connection.html",
                 controller: 'NoConnectionCtrl'
             }
         }
     })

             .state('app.profile', {
                 url: "/profile",
                 views: {
                     'menuContent': {
                         templateUrl: "",
                         controller: ''
                     }
                 }
             })

            .state('app.activities', {
                url: "/activities",
                views: {
                    'menuContent': {
                        templateUrl: "templates/activities.html",
                        controller: 'ActivitiesCtrl'
                    }
                }
            })

            .state('app.myVouchers', {
                url: "/myVouchers",
                views: {
                    'menuContent': {
                        templateUrl: "templates/my-vouchers.html",
                        controller: 'MyVouchersCtrl'
                    }
                }
            })
                .state('app.vouchersDetail', {
                    url: "/vouchersDetail/:id",
                    views: {
                        'menuContent': {
                            templateUrl: "templates/vouchers-detail.html",
                            controller: 'VouchersDetailCtrl'
                        }
                    }
                })
         .state('app.htmls', {
             url: "/html/:page",
             views: {
                 'menuContent': {
                     templateUrl: "templates/html.html",
                     controller: 'HtmlsCtrl'
                 }
             }
         })

     .state('app.registerSuccess', {
         url: "/register/success",
         views: {
             'menuContent': {
                 templateUrl: "templates/success.html",
                 controller: 'RegisterSuccessCtrl'
             }
         }
     })

    .state('app.login', {
        url: "/login/:cpf",
        views: {
            'menuContent': {
                templateUrl: "templates/login.html",
                controller: 'LoginCtrl'
            }
        }
    });


    // if none of the above states are matched, use this as the fallback
    if (window.localStorage.getItem("idUsuario") != undefined) {
        $urlRouterProvider.otherwise('/app/myGames/id');
    }
    else {
        $urlRouterProvider.otherwise('/app/firstAccess');
    }
}).config(function ($ionicConfigProvider) {
    $ionicConfigProvider.views.maxCache(1);


}).filter('object2Array', function () {
    return function (input) {
        var out = [];
        for (i in input) {
            out.push(input[i]);
        }
        return out;
    }
});
