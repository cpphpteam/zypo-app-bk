﻿angular.module('zyppo.controllers', [])

.controller('AppCtrl', function ($ionicPlatform, $scope, $rootScope, $ionicPopup, $ionicModal, $ionicHistory, $timeout, $state) {

    $rootScope.uiRouterState = $state;

    $ionicPlatform.onHardwareBackButton(function() {
        var previousView = $ionicHistory.backView();
        if (!$scope.modalOrder && !$scope.modalProfile && (previousView.stateName == 'app.firstAccess' || previousView.stateName == 'app.recoverPassword' || previousView.stateName == 'app.register' || previousView.stateName == 'app.login')) {
            $rootScope.showFooter = false;
        }
        if ($scope.modalProfile) {
            $scope.modalProfile = false;
        }
        if ($scope.modalOrder) {
            $scope.modalOrder = false;
            $rootScope.showFooter = true;
        }
    });



    $rootScope.$on('$stateChangeSuccess', function () {
        $rootScope.uiRouterState = $state;
        $rootScope.showBackButton = false;
        if ($scope.modalProfile)
        {
            $scope.modalProfile.hide();
            $scope.modalProfile = false;
        }
            
    });

    $ionicModal.fromTemplateUrl('modal-order-game.html', {
        scope: $scope,
        animation: 'slide-in-up'
    }).then(function (modal) {
        $scope.modal = modal;
    });


    $scope.orderGameByName = function () {
        $scope.modal.hide();
        $scope.modalOrder = false;
        $rootScope.showFooter = true;

        $state.go('app.myGames', { orderBy: 'nome' });
    }

    $scope.orderGameByDate = function () {
        $scope.modal.hide();
        $scope.modalOrder = false;
        $rootScope.showFooter = true;
        $state.go('app.myGames', { orderBy: 'data_registro' });
    }

    $scope.vouchersClick = function () {
        $ionicHistory.clearCache();
        $state.go('app.myVouchers', {}, { reload: true });
    }
    $scope.myGamesClick = function () {
        $ionicHistory.clearCache();
        $state.go('app.myGames', {}, { reload: true });
    }

    $rootScope.$on("profileClick", function () {
        $scope.profileClick();
    });

    $scope.profileClick = function () {
        $ionicModal.fromTemplateUrl('templates/profile.html', {
            animation: 'slide-in-up'
        }).then(function (modal) {
            $scope.modalProfile = modal;
            $scope.modalProfile.show();
            $rootScope.uiRouterState = { current: { name: 'app.profile' } };
        });
    }

    $scope.activitiesClick = function () {
        $ionicHistory.clearCache();
        $state.go('app.activities', {}, { reload: true });
    }

    $scope.back = function () {
        $ionicHistory.clearCache();
        $ionicHistory.goBack();
    }

    $scope.orderClick = function () {
        $scope.modal.show();
        $scope.modalOrder = true;
        $rootScope.showFooter = false;
    }
});