﻿angular.module('zyppo.controllers.noConnection', [])

.controller('NoConnectionCtrl', function ($ionicHistory, $rootScope, $scope, $ionicSideMenuDelegate, $ionicPopup, $state, ConnectionService) {
 
    $scope.tryAgain = function () {
        ConnectionService.isConnected().then(function (connected) {
            if (connected) {
                $ionicHistory.goBack();
            }
        });
    }

})