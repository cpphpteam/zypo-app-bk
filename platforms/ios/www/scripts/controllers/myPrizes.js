﻿angular.module('zyppo.controllers.myPrizes', [])

.controller('MyPrizes', function ($stateParams, $rootScope, $scope, $ionicSideMenuDelegate, $ionicPopup, $state, PrizesService) {
    var idGame = $state.params.id;
    var idUser = window.localStorage.getItem("idUsuario");
    $rootScope.showBackButton = true;

    PrizesService.getById(idUser, idGame).then(function (response) {
        $scope.game = response.data;
    });

    $scope.validatePoints = function (userPoint, gamePoint) {
        return parseInt(gamePoint) > parseInt(userPoint);
    }

    $scope.getVoucher = function (idPrize, name) {
        $ionicPopup.confirm({
            title: 'Confirmar resgate!',
            template: '<p><b>' + name + '</b></p>Ao confirmar o resgate será gerado um voucher que poderá ser consultado na opção vouchers e apresentado ao dono do jogo para troca dos seus pontos.',
            scope: $scope,
            buttons: [
              {
                  text: 'Confirmar',
                  type: 'button-positive',
                  onTap: function (e) {
                      PrizesService.create(idPrize, idUser).then(function (response) {
                          if (response.data.success) {
                              $ionicPopup.alert({
                                  title: 'Solicitação finalizada',
                                  template: 'Seu voucher foi criado com sucesso..'
                              });
                              PrizesService.getById(idUser, idGame).then(function (response) {
                                  $scope.game = response.data;
                              });

                          }
                      });
                  }
              },
              { text: 'Cancelar' },
            ]
        });
    }
})