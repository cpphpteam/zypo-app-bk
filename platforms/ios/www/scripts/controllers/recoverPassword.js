﻿angular.module('zyppo.controllers.recoverPassword', [])

.controller('RecoverPassword', function ($scope, $ionicSideMenuDelegate, AccountService, $ionicPopup, $state) {
    $ionicSideMenuDelegate.canDragContent(false);
    $scope.account = {};
    $scope.recover = function () {
        AccountService.recoverPassword($scope.account.email).then(function (result) {        
        });
        $ionicPopup.alert({
            title: 'Estamos quase lá:',
            template: 'Foi enviado ao seu e-mail instruções para redefinir a sua senha.'
        });
    }

    $scope.back = function () {
        $state.go("app.firstAccess");
    }

})