﻿angular.module('zyppo.services.htmlsService', [])

    .factory('HtmlsService', function ($q, $http, $ionicLoading, CacheService) {

        return {
            getAbout: function () {
                var deferred = $q.defer();
                CacheService.getCache('myHtmls', deferred).then(function (hasCache) {
                    if (!hasCache) {
                        $ionicLoading.show({
                            template: 'Carregando...',
                            duration: 3000
                        });
                        $http.post('http://zypo.herokuapp.com/index/sobre').then(function (response) {
                            deferred.resolve(response.data);
                            CacheService.setCache('getAbout', response.data);
                            $ionicLoading.hide();
                        })
                    }
                });
                return deferred.promise;
            },

            getTerms: function () {
                var deferred = $q.defer();
                CacheService.getCache('myHtmls', deferred).then(function (hasCache) {
                    if (!hasCache) {
                        $ionicLoading.show({
                            template: 'Carregando...',
                            duration: 3000
                        });
                        $http.post('http://zypo.herokuapp.com/index/termo').then(function (response) {
                            deferred.resolve(response.data);
                            CacheService.setCache('getTerms', response.data);
                            $ionicLoading.hide();
                        })
                    }
                });
                return deferred.promise;
            },
        }

    });