﻿angular.module('zyppo.controllers.myVouchers', [])

.controller('MyVouchersCtrl', function ($rootScope, $scope, $ionicSideMenuDelegate, $ionicPopup, $state, VoucherService) {
    $scope.activeVouchers = true;
    var id = window.localStorage.getItem("idUsuario");

    VoucherService.listByUser(id).then(function (response) {
        $scope.vouchers = response.data;
    });

    $scope.usedVouchersClick = function () {
        $scope.activeVouchers = false;
    }

    $scope.activeVouchersClick = function () {
        $scope.activeVouchers = true;
    }

    $scope.voucherClick = function (id) {
        $state.go('app.vouchersDetail', { id: id });
    }
})