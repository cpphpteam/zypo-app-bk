﻿angular.module('zyppo.controllers.register', [])
.controller('RegisterSuccessCtrl', function ($scope, $ionicSideMenuDelegate, FirstAccessService, $ionicPopup, $state, AccountService) {
    setTimeout(function () {
        $state.go('app.myGames');
    }, 3000);
})
.controller('RegisterCtrl', function ($rootScope, $scope, $ionicHistory, $ionicSideMenuDelegate, FirstAccessService, $ionicPopup, $state, AccountService) {
    $ionicSideMenuDelegate.canDragContent(false);
    $scope.account = {};
    $scope.cpf = $state.params.cpf;

    $scope.back = function () {
        $rootScope.$emit("profileClick", {});
    }

    if ($scope.cpf == 'edit') {
        $scope.titulo = "Editar perfil"
        $rootScope.showBackButton = true;
        var id = window.localStorage.getItem("idUsuario");
        $scope.edit = true;

        AccountService.getUser(id).then(function (response) {
            $scope.cpf = response.data.cpf;
            $scope.account.fullName = response.data.nome;
            $scope.account.email = response.data.email;
            var data = new Date(response.data.data_nascimento);
            data.setDate(data.getDate() + 1);
            $scope.account.birthDate = formatDate(data);
        });
    }
    else {
        $scope.titulo = "Cadastro"

        $scope.loginFacebook = function () {
            facebookConnectPlugin.login(["public_profile", "email"], function (response) {
                facebookConnectPlugin.api(response.authResponse.userID + "/?fields=name,email,picture", ["public_profile", "email"],
                function (userResponse) {
                    var nome = userResponse.name.split(" ")[0];
                    var sobrenome = userResponse.name.split(" ")[1];
                    var email = userResponse.email;
                    var id = response.authResponse.userID;
                    AccountService.authenticationFacebook(id, $scope.cpf, email).then(function (response) {
                        if (typeof response.data === 'object') {
                            window.localStorage.setItem("idUsuario", response.data.id);
                            $state.go('app.registerSuccess');
                        }
                        else {
                            $ionicPopup.alert({
                                title: 'Ocorreu um erro no cadastro:',
                                template: response.data
                            });
                        }

                    });

                }, function loginError(error) {
                });

            });
        }

    }

    function formatDate(dataStr) {
        var data = new Date(dataStr);

        var dia = data.getDate();
        if (dia < 10) {
            dia = "0" + dia;
        }

        var mes = data.getMonth() + 1;
        if (mes < 10) {
            mes = "0" + mes;
        }

        var ano = data.getFullYear();
        dataFormatada = dia + "/" + mes + "/" + ano;
        return dataFormatada;
    }

    $scope.showDatePicker = function () {
        var options = {
            date: new Date(),
            mode: 'date',
            titleText: 'Data de nascimento',
            androidTheme: 3,
            maxDate: (new Date()).valueOf()
        };



        function onSuccess(date) {
            var formatedDate = formatDate(date);
            $scope.$apply(function () {
                $scope.account.birthDate = formatedDate;
            });
        }

        datePicker.show(options, onSuccess, function () {

        });
    }

    $scope.register = function () {
        if (new Date($scope.account.birthDate) >= new Date()) {
            $ionicPopup.alert({
                title: 'Ocorreu um erro ao cadastrar:',
                template: 'Verifique sua data de nascimento.'
            });
        }
        else {
            var id = window.localStorage.getItem("idUsuario");
            var account = {
                id: id,
                nome: $scope.account.fullName,
                email: $scope.account.email,
                senha: $scope.account.password,
                confirmsenha: $scope.account.confirmPassword,
                data_nascimento: $scope.account.birthDate,
                //cpf: $scope.cpf,
            };

            if ($scope.edit) {
                AccountService.edit(account).then(function (result) {
                    if (result.data.error != undefined) {
                        $ionicPopup.alert({
                            title: 'Ocorreu um erro ao cadastrar:',
                            template: result.data.error
                        });
                    }
                    else {
                        $ionicPopup.alert({
                            title: 'Alteração concluída',
                            template: 'Seu perfil foi alterado com sucesso'
                        });
                    }
                });
            }
            else {
                AccountService.save(account).then(function (result) {
                    if (result.data.error != undefined) {
                        $ionicPopup.alert({
                            title: 'Ocorreu um erro ao cadastrar:',
                            template: result.data.error
                        });
                    }
                    else {
                        AccountService.authentication(account.cpf, account.senha).then(function (result) {
                            window.localStorage.setItem("idUsuario", result.data.id);
                            $state.go('app.registerSuccess');
                        });
                    }
                });
            }

        }

    }
})