﻿angular.module('zyppo.controllers.firstAccess', [])

.controller('FirstAccessCtrl', function ($rootScope, $scope, $ionicSideMenuDelegate, FirstAccessService, $ionicPopup, $state) {
    //$state.go('app.myGames');
    $rootScope.showFooter = false;
    $scope.cpf = "";
    $ionicSideMenuDelegate.canDragContent(false);
    $scope.validateCpf = function () {
        if (validateCpf($scope.cpf.replace('.', '').replace('.', '').replace('-', '')))
        {
            FirstAccessService.checkAccount($scope.cpf).then(function (result) {
                if (result == 'not_registered') {
                    $state.go('app.register', { cpf: $scope.cpf });
                }
                else if (result == 'registered') {
                    $state.go('app.login', { cpf: $scope.cpf });
                }
            });
        }
        else {
            $ionicPopup.alert({
                title: 'Verifique os dados abaixo:',
                template: 'O CPF não foi digitado corretamente.'
            });
        }
        
    }

    $scope.contactClick = function () {
        window.open('http://zypo.com.br/contato', '_blank', 'location=yes');
    }

    function validateCpf(cpf) {
        var numeros, digitos, soma, i, resultado, digitos_iguais;
        digitos_iguais = 1;
        if (cpf.length < 11)
            return false;
        for (i = 0; i < cpf.length - 1; i++)
            if (cpf.charAt(i) != cpf.charAt(i + 1)) {
                digitos_iguais = 0;
                break;
            }
        if (!digitos_iguais) {
            numeros = cpf.substring(0, 9);
            digitos = cpf.substring(9);
            soma = 0;
            for (i = 10; i > 1; i--)
                soma += numeros.charAt(10 - i) * i;
            resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
            if (resultado != digitos.charAt(0))
                return false;
            numeros = cpf.substring(0, 10);
            soma = 0;
            for (i = 11; i > 1; i--)
                soma += numeros.charAt(11 - i) * i;
            resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
            if (resultado != digitos.charAt(1))
                return false;
            return true;
        }
        else
            return false;
    }
})