﻿angular.module('zyppo.controllers.login', [])

.controller('LoginCtrl', function ($scope, $ionicSideMenuDelegate, AccountService, $ionicPopup, $state) {
    $ionicSideMenuDelegate.canDragContent(false);

    $scope.cpf = $state.params.cpf;
    $scope.account = {};

    $scope.loginFacebook = function () {
        facebookConnectPlugin.login(["public_profile", "email"], function (response) {
            facebookConnectPlugin.api(response.authResponse.userID + "/?fields=name,email,picture", ["public_profile", "email"],
            function (userResponse) {
                var nome = userResponse.name.split(" ")[0];
                var sobrenome = userResponse.name.split(" ")[1];
                var email = userResponse.email;
                var id = response.authResponse.userID;
                AccountService.authenticationFacebook(id, $scope.cpf, email).then(function (response) {
                    if (typeof response.data === 'object') {                        
                        window.localStorage.setItem("idUsuario", response.data.id);
                        $state.go('app.myGames');
                    }
                    else {
                        $ionicPopup.alert({
                            title: 'Ocorreu um erro no login:',
                            template: response.data
                        });
                    }
                 
                });

            }, function loginError(error) {
                //alert("Ops, ocorreu um erro: " + JSON.stringify(error));
            });

        });
    }


    $scope.contactClick = function () {
        window.open('http://zypo.com.br/contato', '_blank', 'location=yes');
    }


// 70619093153
// peixecru
    $scope.login = function () {
        // $state.go('app.myGames');
        AccountService.authentication($scope.cpf.replace('.', '').replace('.', '').replace('-', ''), $scope.account.password).then(function (result) {
            if (result.data.cpf == undefined) {
                $ionicPopup.alert({
                    title: 'Ocorreu um erro no login:',
                    template: result.data
                });
            }
            else {
                $ionicPopup.alert({
                    title: 'Olá!',
                    template: "Seja bem vindo " + result.data.nome
                });
                window.localStorage.setItem("idUsuario", result.data.id);
                $state.go('app.myGames');
            }
        });
    }

    $scope.forgotPassword = function () {
        $state.go("app.recoverPassword");
    }
})