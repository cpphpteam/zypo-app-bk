﻿angular.module('zyppo.controllers.vouchersDetail', [])

.controller('VouchersDetailCtrl', function ($rootScope, $scope, $ionicSideMenuDelegate, $ionicPopup, $state, VoucherService) {
    $scope.id = $state.params.id;
    $rootScope.showBackButton = true;

    VoucherService.getById($scope.id).then(function (response) {
        $scope.voucher = response.data.voucher;
        $scope.voucher.qrCode = response.data.qrCode;
    });
    $scope.usedVouchersClick = function () {
        $scope.activeVouchers = false;
    }

    $scope.activeVouchersClick = function () {
        $scope.activeVouchers = true;
    }
})