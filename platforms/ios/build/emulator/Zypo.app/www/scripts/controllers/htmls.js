﻿angular.module('zyppo.controllers.htmls', [])

.controller('HtmlsCtrl', function ($ionicHistory, $rootScope, $scope, $ionicSideMenuDelegate, HtmlsService, $ionicPopup, $state, ConnectionService) {
 
    var page = $state.params.page;

    if (page == 'about') {
        HtmlsService.getAbout().then(function (response) {
            $scope.message = response;
        });
    }

    else if (page == 'terms') {
        HtmlsService.getTerms().then(function (response) {
            $scope.message = response;
        });
    }

})